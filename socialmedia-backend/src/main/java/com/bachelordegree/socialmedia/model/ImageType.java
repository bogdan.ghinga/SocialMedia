package com.bachelordegree.socialmedia.model;

public enum ImageType {
    PROFILE,
    BACKGROUND
}