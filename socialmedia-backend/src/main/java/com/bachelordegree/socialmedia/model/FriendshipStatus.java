package com.bachelordegree.socialmedia.model;

public enum FriendshipStatus {
    PENDING,
    ACCEPTED,
    NONE,
}